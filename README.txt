
Overview
--------
nocase is a simple module that allows for case insensitive Drupal passwords.

Author(s)
---------
Karthik Kumar / Zen / |gatsby| : http://drupal.org/user/21209

Links
-----
Project page: http://drupal.org/project/nocase
